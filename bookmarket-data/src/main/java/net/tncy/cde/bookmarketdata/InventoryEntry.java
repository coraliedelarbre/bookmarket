package net.tncy.cde.bookmarketdata;

public class InventoryEntry {
    public Book book;
    public Integer quantity;
    public float averagePrice;
    public float currentPrice;

    public InventoryEntry(Book book, Integer quantity, float averagePrice, float currentPrice) {
        this.book = book;
        this.quantity = quantity;
        this.averagePrice = averagePrice;
        this.currentPrice = currentPrice;
    }
}