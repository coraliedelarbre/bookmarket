package net.tncy.cde.bookmarketdata;

import java.util.List;

public class Bookstore {
    public Integer id;
    public String name;
    public List<InventoryEntry> inventoryEntry;

    public Bookstore (Integer id, String name, List<InventoryEntry> inventoryEntry) {
        this.id = id;
        this.name = name;
        this.inventoryEntry = inventoryEntry;
    }
}