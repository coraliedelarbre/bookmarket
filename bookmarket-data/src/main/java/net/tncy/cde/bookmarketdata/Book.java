package net.tncy.cde.bookmarketdata;

import net.tncy.cde.validation.ISBN;

public class Book {
    public Integer id;
    public String title;
    public String author;
    public String publisher;

    @ISBN
    public String isbn;

    public Book(Integer id, String title, String author, String publisher, String isbn){
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.isbn = isbn;
    }
}