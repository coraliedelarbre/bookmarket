package net.tncy.cde.bookmarket.services;

import java.util.ArrayList;
import java.util.List;
import net.tncy.cde.bookmarketdata.Book;
import net.tncy.cde.bookmarketdata.Bookstore;
import net.tncy.cde.bookmarketdata.InventoryEntry;

public class BookstoreService {
    public List<Book> listBooks (Bookstore bookstore){
        List<Book> books = new ArrayList<>();
        for(InventoryEntry i : bookstore.inventoryEntry) {
            books.add(i.book);
        }
        return books;
    }
}
